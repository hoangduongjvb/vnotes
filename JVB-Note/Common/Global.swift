//
//  Config.swift
//  JVB-Note
//
//  Created by JVB_IOS on 22/12/2021.
//
import Foundation
import UIKit
import CoreData

let appDelegate = UIApplication.shared.delegate as! AppDelegate

extension NSObject {
    static var typeName: String {
        return String(describing: self)
    }
    var objectName: String {
        return String(describing: type(of: self))
    }
}
extension TimeInterval {
    var time: String {
        let hours = Int((self/3600))
        let minutes = Int(truncatingRemainder(dividingBy: 60*60)/60)
        let seconds = Int(truncatingRemainder(dividingBy: 60))
        return String(format:"%02dh-%02dm-%02ds", hours, minutes, seconds)
    }
}
func convertDateToString(date: Date) -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = "EEEE dd-MM-yyyy HH:mm:ss a"
    return formatter.string(from: date)
}
