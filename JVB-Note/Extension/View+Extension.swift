//
//  View+Extension.swift
//  JVB-Note
//
//  Created by JVB_IOS on 28/10/2021.
//

import Foundation
import UIKit

extension UIView {
    func getAllConstraints() -> [NSLayoutConstraint]? {
        var views: [AnyHashable] = []
        var view = self
        views.append(view)
        while (view.superview != nil) {
            if let superview = view.superview {
                views.append(superview)
            }
            view = view.superview!
        }
        var constraints: [NSLayoutConstraint] = []
        for view in views {
            guard let view = view as? UIView else {
                continue
            }
            let arr = (view.constraints as NSArray).filtered(using: NSPredicate(block: { object, bindings in
                return (object as AnyObject).firstItem as? UIView == self || (object as AnyObject).secondItem as? UIView == self
            }))
            
            if let arr = arr as? [NSLayoutConstraint] {
                constraints.append(contentsOf: arr)
            }
        }
        return constraints
    }
    func bottomConstraint(_ secondView: UIView? = nil) -> NSLayoutConstraint? {
        guard let constraints = getAllConstraints() else {return nil}
        var constraint: NSLayoutConstraint? = nil
        for cons in constraints {
            let firstItem = cons.firstItem as? UIView
            let secondItem = cons.secondItem as? UIView
            if firstItem == self && cons.firstAttribute == .bottom {
                if secondItem ==  nil {
                    constraint = cons
                } else if !secondItem!.isDescendant(of: firstItem!) {
                    if secondView == nil {
                        constraint = cons
                    } else if secondView != nil && secondItem == secondView {
                        constraint = cons
                    }
                }
            } else if secondItem == self && cons.secondAttribute == .bottom {
                if firstItem == nil {
                    constraint = cons
                } else if !firstItem!.isDescendant(of: secondItem!) {
                    if secondView == nil {
                        constraint = cons
                    } else if secondItem != nil && firstItem == secondView {
                        constraint = cons
                    }
                }
            }
        }
        return constraint
    }
    
    func configCustomView(_ container: UIView!) -> Void {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.frame = container.frame
        container.backgroundColor = .clear
        container.addSubview(self)
        NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: container, attribute: .leading, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: container, attribute: .trailing, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: container, attribute: .top, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: container, attribute: .bottom, multiplier: 1.0, constant: 0).isActive = true
    }
    func setupView() {
        let background: UIImageView = {
            let backgroundImage = UIImageView(frame: .zero)
            backgroundImage.image = UIImage(named: "background")
            backgroundImage.contentMode = .scaleAspectFill
            backgroundImage.translatesAutoresizingMaskIntoConstraints = false
            return backgroundImage
        }()
        insertSubview(background, at: 0)
        NSLayoutConstraint.activate([
            background.topAnchor.constraint(equalTo: topAnchor),
            background.leadingAnchor.constraint(equalTo: leadingAnchor),
            background.trailingAnchor.constraint(equalTo: trailingAnchor),
            background.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
}
