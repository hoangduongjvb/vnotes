//
//  NoteHeader.swift
//  JVB-Note
//
//  Created by JVB_IOS on 02/11/2021.
//

import UIKit

protocol NoteHeaderDelegate {
    func actionSearch()
    func displayTableView()
    func displayCollectionView()
    func filterAndSort()
}



class NoteHeader: UIView {
    
    @IBOutlet var contentView: UIView!
    
    var delegate: NoteHeaderDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
       
    }
    func commonInit() {
      Bundle.main.loadNibNamed("NoteHeader", owner: self, options: nil)
        contentView.configCustomView(self)
        customViewButton()
    }
    
    func customViewButton() {
      
    }
    
    @IBAction func onClickSearchNote(_ sender: UIButton) {
        self.delegate?.actionSearch()
    }
    @IBAction func onDisplayTableView(_ sender: UIButton) {
        self.delegate?.displayTableView()
    }
    @IBAction func onDisplayCollectionView(_ sender: UIButton) {
        self.delegate?.displayCollectionView()
    }
    @IBAction func onFilterAndSort(_ sender: UIButton) {
        self.delegate?.filterAndSort()
    }
    
}
