//
//  NoteTableViewCell.swift
//  JVB-Note
//
//  Created by Hoàng Dương on 04/11/2021.
//

import UIKit

class NoteTableViewCell: UITableViewCell {

    @IBOutlet weak var contentViewCell: UIView!
    @IBOutlet weak var titleNoteTV: UILabel!
    @IBOutlet weak var timeNoteTV: UILabel!
    @IBOutlet weak var notifiTV: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        contentViewCell.layer.cornerRadius = 10.0
        
    }
}
