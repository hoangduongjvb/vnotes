//
//  NoteTableView.swift
//  JVB-Note
//
//  Created by Hoàng Dương on 04/11/2021.
//

import UIKit

class NoteTableView: UIView, CreateNoteDelegate {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    var data: [Note] = []
    var noteCoreData = NoteCoreData()

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    func doneNoteDelegate(note: Note) {
        noteCoreData.saveNoteData(note: note)
        noteCoreData.fetchNoteData()
    }
    func trashNoteDelegate() {
        noteCoreData.fetchNoteData()
    }
    
    func commonInit() {
        
        Bundle.main.loadNibNamed("NoteTableView", owner: self, options: nil)
        contentView.configCustomView(self)
        tableView.register(UINib(nibName: "NoteTableViewCell", bundle: nil), forCellReuseIdentifier: "NoteTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }
    
}
extension NoteTableView: UITableViewDataSource,UITableViewDelegate {
    

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let note = data[indexPath.row]
        let tableViewCell: NoteTableViewCell = tableView.dequeueReusableCell(withIdentifier: "NoteTableViewCell") as! NoteTableViewCell
        tableViewCell.titleNoteTV.text = note.title_note
        tableViewCell.timeNoteTV.text = convertDateToString(date: note.time_note)
        if note.notify == true {
            tableViewCell.notifiTV.image = UIImage(named: "iconNotifi")
        } else {
            tableViewCell.notifiTV.isHidden = true
        }
        return tableViewCell
    }
}
