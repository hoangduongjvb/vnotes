//
//  ItemsCreateNote.swift
//  JVB-Note
//
//  Created by Hoàng Dương on 04/11/2021.
//

import UIKit

class ItemsCreateNote: UIView {

    @IBOutlet var contentView: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed("ItemsCreateNote", owner: self, options: nil)
        contentView.configCustomView(self)
    }

}
