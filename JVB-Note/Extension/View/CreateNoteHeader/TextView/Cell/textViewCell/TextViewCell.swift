//
//  CreateNoteCell.swift
//  JVB-Note
//
//  Created by Hoàng Dương on 12/11/2021.
//

import UIKit
import NotesTextView

class TextViewCell: UITableViewCell {
    

    @IBOutlet weak var textView: NotesTextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
        
    }
    
}
