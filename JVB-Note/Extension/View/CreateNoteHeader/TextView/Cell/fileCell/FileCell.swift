//
//  FileCell.swift
//  JVB-Note
//
//  Created by Hoàng Dương on 13/11/2021.
//

import UIKit

class FileCell: UITableViewCell {
    
    
    @IBOutlet weak var titleFile: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
