//
//  SetTimeTableViewCell.swift
//  JVB-Note
//
//  Created by JVB_IOS on 22/12/2021.
//

import UIKit

class SetTimeTableViewCell: UITableViewCell {

    @IBOutlet weak var dayBeforeNotify: UITextField!
    @IBOutlet weak var datePicker: UIDatePicker!
    var timer = Timer()
    var forNotice: ((Date) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupTextField()
        
    }
 
    func setupTextField() {
        dayBeforeNotify.isEnabled = false
        dayBeforeNotify.backgroundColor = .clear
    }
    @IBAction func valueChanged(_ sender: UIDatePicker) {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
        forNotice?(datePicker.date)
    }
   @objc func updateCounter() {
       var timeLeft = datePicker.date.timeIntervalSinceNow
       if timeLeft < 1 {
           timeLeft = 0
           dayBeforeNotify.text = timeLeft.time
           timer.invalidate()
       } else {
           dayBeforeNotify.text = timeLeft.time
       }
    }
  

}
extension SetTimeTableViewCell: UITextFieldDelegate {

    
}


