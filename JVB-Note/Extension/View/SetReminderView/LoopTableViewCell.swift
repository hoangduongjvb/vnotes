//
//  LoopTableViewCell.swift
//  JVB-Note
//
//  Created by JVB_IOS on 22/12/2021.
//

import UIKit

class LoopTableViewCell: UITableViewCell {
    
    @IBOutlet weak var loopSwitch: UISwitch!
    var onLoop: ((Bool) -> Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    @IBAction func optionLoop(_ sender: UISwitch) {
        onLoop?(sender.isOn)
    }
    
}
