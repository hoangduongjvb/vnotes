//
//  NotificationTableViewCell.swift
//  JVB-Note
//
//  Created by JVB_IOS on 22/12/2021.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {

    @IBOutlet weak var switchNotify: UISwitch!
    var onReminder: ((Bool) -> Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    @IBAction func valueChanged(_ sender: UISwitch) {
        onReminder?(sender.isOn)
    }
}
