//
//  NoteCollectionViewCell.swift
//  JVB-Note
//
//  Created by Hoàng Dương on 04/11/2021.
//

import UIKit

class NoteCollectionViewCell: UICollectionViewCell {

    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var timerNote: UILabel!
    @IBOutlet weak var notifyNote: UIImageView!
    @IBOutlet weak var hourNote: UILabel!
    

    
    @IBOutlet weak var shadowView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        customTableView()
    }
    func customTableView() {
        tableView.isScrollEnabled = false
        tableView.dataSource = self
        tableView.delegate = self
        tableView.reloadData()
        tableView.register(UINib(nibName: "NotifyTableViewCell", bundle: nil), forCellReuseIdentifier: "NotifyTableViewCell")
    }
   
    
}

extension NoteCollectionViewCell: UITableViewDataSource,UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotifyTableViewCell", for: indexPath) as! NotifyTableViewCell
        cell.backgroundColor = .systemBlue
        return cell
    }
    
    
}
