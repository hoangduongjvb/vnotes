//
//  NoteCollectionView.swift
//  JVB-Note
//
//  Created by Hoàng Dương on 04/11/2021.
//

import UIKit

class NoteCollectionView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed("NoteCollectionView", owner: self, options: nil)
        contentView.configCustomView(self)
        
        
        
        collectionView.register(UINib(nibName: "NoteCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "NoteCollectionViewCell")
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.reloadData()
        
        
    }
    
}

extension NoteCollectionView: UICollectionViewDelegateFlowLayout,UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       return 3
   }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NoteCollectionViewCell", for: indexPath as IndexPath) as! NoteCollectionViewCell
        cell.timerNote.text = "11:20"
        cell.notifyNote.isHidden = false
        cell.hourNote.isHidden =  false
        
        
        cell.shadowView.layer.cornerRadius = 10
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let sectionInset = (collectionViewLayout as! UICollectionViewFlowLayout).sectionInset
        let referenceHeight = collectionView.safeAreaLayoutGuide.layoutFrame.height
        let referenceWidth: CGFloat = 150
        - sectionInset.top
        - sectionInset.bottom
        - collectionView.contentInset.top
        - collectionView.contentInset.bottom
        return CGSize(width: referenceWidth, height: referenceHeight)
    }
}
