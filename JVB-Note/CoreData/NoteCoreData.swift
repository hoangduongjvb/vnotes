//
//  NoteCoreData.swift
//  JVB-Note
//
//  Created by JVB_IOS on 17/12/2021.
//
import UIKit
import CoreData


class NoteCoreData: NSManagedObject {
    var notes = [Note]()
    var note = Note()
    var entities: [NSManagedObject] = []
    func fetchNoteData() {
        appDelegate.notification.notificationCenter.removeAllDeliveredNotifications()
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "NotesData")
        fetchRequest.predicate = NSPredicate(format: "id = %d", note.id as CVarArg)
        do {
            entities = try managedContext.fetch(fetchRequest)
            notes.removeAll()
            if entities.count >= 0 {
                for entity in entities {
                    let newNote = Note()
                    newNote.id = Int(truncating: NSNumber(value: entity.value(forKey: "id") as! Int))
                    newNote.title_note = entity.value(forKey: "title_note") as! String
                    newNote.content_note = entity.value(forKey: "content") as! String
                    newNote.time_note = entity.value(forKey: "time_note") as! Date
                    newNote.notify = entity.value(forKey: "notify") as! Bool
                    newNote.before_note = entity.value(forKey: "before_note") as! Date
                    newNote.loop = entity.value(forKey: "loop") as! Bool
                    newNote.create_note = entity.value(forKey: "create_note") as! Date
                    if newNote.notify == true {
                    }
                    notes.append(newNote)
                }
            }
        } catch let error as NSError{
            print("Could not fetch.\(error), \(error.userInfo)")
        }
    }
    
    func saveNoteData(note: Note) {
        let managedContext = appDelegate.persistentContainer.viewContext
       let noteEnity = NSEntityDescription.entity(forEntityName: "NotesData", in: managedContext)
        let saveNote = Note(entity: noteEnity!, insertInto: managedContext)
        saveNote.id = notes.count + 1
        saveNote.time_note = note.time_note
        saveNote.title_note = note.title_note
        saveNote.content_note = note.content_note
        saveNote.before_note = note.before_note
        saveNote.notify = note.notify
        saveNote.loop = note.loop
        saveNote.create_note = note.create_note
        do {
            try managedContext.save()
            notes.append(saveNote)
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    func deleteNoteData(id: Int) {
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "NotesData")
        fetchRequest.predicate = NSPredicate(format: "id = %d", "\(id)")
        do {
            let delete = try managedContext.fetch(fetchRequest)
            let objectToDelete = delete[0]
            managedContext.delete(objectToDelete)
            do {
                try managedContext.save()
            } catch {
                print(error)
            }
        } catch {
            print(error)
        }
    }
    func updateNoteData(note: Note) {
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "NotesData")
        fetchRequest.predicate = NSPredicate(format: "id = %d", "\(note.id)")
        do {
            let update = try managedContext.fetch(fetchRequest)
            let objectToUpdate = update[0]
            objectToUpdate.setValue(note.title_note, forKey: "title_note")
            objectToUpdate.setValue(note.content_note, forKey: "content_note")
            objectToUpdate.setValue(note.time_note, forKey: "time_note")
            objectToUpdate.setValue(note.notify, forKey: "notify")
            do {
                try managedContext.save()
            } catch {
                print(error)
            }
        } catch {
            print(error)
        }
    }
}

