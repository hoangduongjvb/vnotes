//
//  Notifications.swift
//  JVB-Note
//
//  Created by JVB_IOS on 20/12/2021.
//

import Foundation
import UserNotifications

class Notifications: NSObject, UNUserNotificationCenterDelegate {
    
    let notificationCenter = UNUserNotificationCenter.current()
    
    func notificationRequset() {
        let options: UNAuthorizationOptions = [.alert, .sound, .badge]
        
        notificationCenter.requestAuthorization(options: options) {
            (didAllow, error) in
            if !didAllow {
                print("User has declined notifications")
            }
        }
    }
    func sheduleNotification(notificationType: String) {
        let content = UNMutableNotificationContent()
        let categoryIdentifire = "Delete Notification Type"
        
        content.title = notificationType
        content.body = "This is example how to create" + notificationType
        content.sound = UNNotificationSound.default
        content.badge = 1
        content.categoryIdentifier = categoryIdentifire
        
    let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
    let identifier = "Local Notification"
    let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        
        notificationCenter.add(request) { (error) in
            if let error = error {
                print("Error \(error.localizedDescription)")
            }
        }
        let snoozeAction = UNNotificationAction(identifier: "Snooze", title: "Snooze", options: [])
        let deleteAction = UNNotificationAction(identifier: "Delete", title: "Delete", options: [.destructive])
        let category = UNNotificationCategory(identifier: categoryIdentifire, actions: [snoozeAction, deleteAction], intentIdentifiers: [], options: [])
        notificationCenter.setNotificationCategories([category])
    }
    func setupLocalNotification(note: Note) {
        let calendar = Calendar.current
        let content = UNMutableNotificationContent()
        let categoryIdentifier = "Note Notification"
        
        content.title = note.title_note
//        content.body = note.content_note
        content.sound = UNNotificationSound.default
        content.badge = 1
        content.categoryIdentifier = categoryIdentifier
        
        var dateComponent = DateComponents()
        dateComponent.year = calendar.component(.year, from: note.time_note)
        dateComponent.month = calendar.component(.month, from: note.time_note)
        dateComponent.day = calendar.component(.day, from: note.time_note)
        dateComponent.hour = calendar.component(.hour, from: note.time_note)
        dateComponent.minute = calendar.component(.minute, from: note.time_note)
        dateComponent.second = calendar.component(.second, from: note.time_note)
        
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponent, repeats: false)
        let identifier = "Free Book Local Notification"
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        
        appDelegate.notification.notificationCenter.add(request) { (error) in
            if let error = error {
                print("Error \(error.localizedDescription)")
            }
        }
    }
}
