//
//  CreateNoteViewController.swift
//  JVB-Note
//
//  Created by Hoàng Dương on 04/11/2021.
//

import UIKit
import NotesTextView

protocol CreateNoteDelegate {
    func doneNoteDelegate(note: Note)
    func trashNoteDelegate()
}


class CreateNoteViewController: UIViewController {
    
    @IBOutlet weak var CreNoteHeader: ItemsCreateNote!
    @IBOutlet weak var boxEditNote: UIView!
    @IBOutlet weak var dateTF: UITextField!
    @IBOutlet weak var titleTF: UITextField!
    let textView = NotesTextView()
    var noteNotify: Bool = true
    var noteLoop: Bool = true
    var timerNotify: Date = Date()
    var dataNote: ((Note?) -> Void)?
    
    var delegate: CreateNoteDelegate!
    var note = Note()
    override func viewDidLoad() {
        super.viewDidLoad()
        setTime()
  
        view.addSubview(textView)
                textView.translatesAutoresizingMaskIntoConstraints = false
                textView.topAnchor.constraint(equalTo: boxEditNote.topAnchor).isActive = true
                textView.leadingAnchor.constraint(equalTo: boxEditNote.leadingAnchor).isActive = true
                textView.trailingAnchor.constraint(equalTo: boxEditNote.trailingAnchor).isActive = true
                textView.bottomAnchor.constraint(equalTo: boxEditNote.bottomAnchor).isActive = true
                // to adjust the content insets based on keyboard height
                textView.shouldAdjustInsetBasedOnKeyboardHeight = true
                textView.layer.opacity = 0.3
                
                // to support iPad
                textView.hostingViewController = self
                
                let _ = textView.becomeFirstResponder()
        
    }
    func dataLine() {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE, dd/MM/yyyy HH:mm:ss a"
        let date = formatter.date(from: dateTF.text!)
        
        let currentDate = Date()
        note.title_note = titleTF.text ?? ""
        note.content_note = textView.text ?? ""
        note.create_note = currentDate
        note.time_note = date!
        note.notify = noteNotify
        note.loop = noteLoop
        note.before_note = timerNotify
    }

    func setTime() {
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .dateAndTime
        datePicker.addTarget(self, action: #selector(dateChanged(datePicker:)), for: UIControl.Event.valueChanged)
        datePicker.frame.size = CGSize(width: 0, height: 300)
        datePicker.preferredDatePickerStyle = .wheels
        dateTF.textAlignment = .center
        dateTF.inputView = datePicker
        dateTF.text = formatDate(date: Date())
    }
    func formatDate(date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE, dd/MM/yyyy HH:mm:ss a"
        return formatter.string(from: date)
    }
    @objc func dateChanged(datePicker: UIDatePicker) {
        dateTF.text = formatDate(date: datePicker.date)
    }
    @IBAction func actionPressDone(_ sender: Any) {
     
       
        if titleTF.text!.isEmpty, textView.text!.isEmpty {
            self.dismiss(animated: true)
        } else {
            dataLine()
            self.dataNote?(note)
            self.delegate?.doneNoteDelegate(note: note)
            self.dismiss(animated: false)
        }
    }
    @IBAction func actionPressTrash(_ sender: Any) {
        self.delegate?.trashNoteDelegate()
        let alert = UIAlertController(title: "Delete This Note", message: "After delete this note, you can't restore it", preferredStyle: .alert)
        let actionOK = UIAlertAction(title: "OK", style: .destructive) {_ in
            self.dismiss(animated: true, completion: nil)
        }
        let actionCancel = UIAlertAction(title: "Cancel", style: .cancel) {_ in
            print("Cancel")
        }
        alert.addAction(actionOK)
        alert.addAction(actionCancel)
        self.present(alert, animated: true) {
        }
    }
    @IBAction func actionSetReminder(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "setReminderViewController") as! SetReminderViewController
        vc.sendData = { status1, status2, result in
            self.noteNotify = status1
            self.noteLoop = status2
            self.timerNotify = result
        }
        present(vc, animated: true, completion: nil)
    }
    @IBAction func actionPressUndo(_ sender: Any) {
    }
    @IBAction func actionPressForward(_ sender: Any) {
    }

}
