//
//  RootViewController.swift
//  JVB-Note
//
//  Created by Hoàng Dương on 03/11/2021.
//

import UIKit


class NoteViewController: HomeViewController {

    @IBOutlet weak var searchNote: UISearchBar!
    @IBOutlet weak var noteHeader: NoteHeader!
    @IBOutlet weak var baseTableView: NoteTableView!
    @IBOutlet weak var baseCollectionView: NoteCollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        noteHeader.delegate = self
       setupFullView()
    }
  
    func setupFullView() {
        searchNote.layer.cornerRadius = 10
        baseTableView.layer.cornerRadius = 10
        view.backgroundColor = .clear
        searchNote.isHidden = true
        baseCollectionView.isHidden = false
        baseTableView.isHidden = true
        if noteData != nil {
            baseTableView.data.append(noteData ?? Note())
            baseTableView.tableView.reloadData()
        }
    }
}
extension NoteViewController: NoteHeaderDelegate {
    func displayTableView() {
        baseTableView.isHidden = false
        baseCollectionView.isHidden = true
    }
    
    func displayCollectionView() {
        baseTableView.isHidden = true
        baseCollectionView.isHidden = false
        
     
    }
    
    func filterAndSort() {
        
    }
    
    func actionSearch() {
        searchNote.isHidden = !searchNote.isHidden
    }
    
    
}
