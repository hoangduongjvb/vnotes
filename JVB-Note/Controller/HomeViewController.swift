//
//  HomeViewController.swift
//  JVB-Note
//
//  Created by Hoàng Dương on 03/11/2021.
//
import UIKit
class HomeViewController: UIViewController {
    @IBOutlet weak var TabbarView: UIView!
    @IBOutlet weak var contentView: UIView!
    var noteCoreData = NoteCoreData()
    var noteData: Note?
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
        override var preferredStatusBarStyle: UIStatusBarStyle {
            return .lightContent
        }
        
        lazy var noteScreen: NoteViewController = {
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "NoteViewController") as! NoteViewController
            viewController.noteData = noteData
            self.add(asChildViewController: viewController)
            
            return viewController
        }()
        
        lazy var calendarScreen: CalendarViewController = {
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "CalendarViewController") as! CalendarViewController
            self.add(asChildViewController: viewController)
            return viewController
        }()
        
        private func add(asChildViewController viewController: UIViewController) {
            addChild(viewController)
            view.addSubview(viewController.view)
            
        viewController.view.frame = view.bounds
        viewController.view.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        
        viewController.didMove(toParent: self)
    }
    private func remove(asChildViewController viewController: UIViewController) {
        viewController.willMove(toParent: nil)
        viewController.view.removeFromSuperview()
        viewController.removeFromParent()
    }
    
    func setupView() {
        let background: UIImageView = {
            let backgroundImage = UIImageView(frame: .zero)
            backgroundImage.image = UIImage(named: "background")
            backgroundImage.contentMode = .scaleAspectFill
            backgroundImage.translatesAutoresizingMaskIntoConstraints = false
            return backgroundImage
        }()
        view.insertSubview(background, at: 0)
        NSLayoutConstraint.activate([
            background.topAnchor.constraint(equalTo: view.topAnchor),
            background.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            background.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            background.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
  
        designTabbar()
    }
    func designTabbar() {
        
        self.TabbarView?.layer.cornerRadius = 8.0
        self.TabbarView?.clipsToBounds = true
    }
    @IBAction func clickTabbar(_ sender: UIButton) {
        let tag = sender.tag
        switch tag {
        case 1:
            remove(asChildViewController: calendarScreen)
            contentView.addSubview(noteScreen.view)
            print(1)
        case 2:
            remove(asChildViewController: noteScreen)
            contentView.addSubview(calendarScreen.view)
            print(2)
        default:
            print("Error")
        }
    }
    @IBAction func addNewNote(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "createNoteViewController") as! CreateNoteViewController
        vc.dataNote = { [self] result in
            noteData = result
            remove(asChildViewController: noteScreen)
            remove(asChildViewController: calendarScreen)
            contentView.addSubview(noteScreen.view)
            noteCoreData.saveNoteData(note: noteData!)
            noteCoreData.fetchNoteData()
        }
        present(vc, animated: true, completion: nil)
    }
    
    
    
}
