//
//  SetReminderViewControllwe.swift
//  JVB-Note
//
//  Created by JVB_IOS on 21/12/2021.
//

import Foundation
import UIKit

class SetReminderViewController: UIViewController {
    
    @IBOutlet weak var topupSetReminder: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var doneButton: UIButton!
    var sendData: ((Bool, Bool, Date) -> Void)?
    var switch1: Bool = true
    var switch2: Bool = true
    var timerNotify: Date = Date()
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    func setup() {
        
        tableView.backgroundView?.layer.opacity = 0.5
        tableView.register(UINib(nibName: "NotificationTableViewCell", bundle: nil), forCellReuseIdentifier: "NotificationTableViewCell")
        tableView.register(UINib(nibName: "SetTimeTableViewCell", bundle: nil), forCellReuseIdentifier: "SetTimeTableViewCell")
        tableView.register(UINib(nibName: "LoopTableViewCell", bundle: nil), forCellReuseIdentifier: "LoopTableViewCell")
        topupSetReminder.layer.cornerRadius = 8.0
        topupSetReminder.clipsToBounds = true
        tableView.dataSource = self
        tableView.delegate = self
        tableView.reloadData()
        
    }
    
    @IBAction func donePress(_ sender: Any) {
        self.dismiss(animated: false) { [weak self] in
            self?.sendData?(self?.switch1 ?? true, self?.switch2 ?? true, (self?.timerNotify)!)
        }
    }
    
}
extension SetReminderViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 45
        case 1:
            return 200
        case 2:
            return 45
        default:
            return 0
            
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            let cell0 = tableView.dequeueReusableCell(withIdentifier: "NotificationTableViewCell", for: indexPath) as! NotificationTableViewCell
            cell0.onReminder = { status in
                cell0.switchNotify.isOn = status
                self.switch1 = status
                tableView.reloadData()
            }
            return cell0
        case 1:
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "SetTimeTableViewCell", for: indexPath) as! SetTimeTableViewCell
            cell1.forNotice = { [self] dates in
                 timerNotify = dates
                tableView.reloadData()
            }
            return cell1
        case 2:
            let cell2 = tableView.dequeueReusableCell(withIdentifier: "LoopTableViewCell", for: indexPath) as! LoopTableViewCell
            cell2.onLoop = { status in
                cell2.loopSwitch.isOn = status
                self.switch2 = status
                tableView.reloadData()
            }
            return cell2
        default:
            let cell0 = tableView.dequeueReusableCell(withIdentifier: "NotificationTableViewCell", for: indexPath) as! NotificationTableViewCell
            return cell0
        }
    }
}
