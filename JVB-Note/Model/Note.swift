//
//  Note.swift
//  JVB-Note
//
//  Created by Hoàng Dương on 05/11/2021.
//


import UIKit
import CoreData

@objc(Note)
class Note: NSManagedObject {
    var id: Int!
    var title_note: String!
    var content_note: String!
    var create_note: Date!
    var time_note: Date!
    var notify: Bool!
    var before_note: Date!
    var loop: Bool!
}

